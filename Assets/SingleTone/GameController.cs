using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingletonPattern
{
    public class GameController : MonoBehaviour
    {

        //Git �׽�Ʈ
        public void TestCsharpSingleton()
        {
            Debug.Log("C#");

            SingletonCsharp instance = SingletonCsharp.Instance;
            instance.TestSingleTon();

            SingletonCsharp instance2 = SingletonCsharp.Instance;
            instance2.TestSingleTon();
        }

        public void TestUnitySingleton()
        {
            Debug.Log("Unity");

            SingletonUnity instance = SingletonUnity.Instance;
            instance.TestSingleTon();

            SingletonUnity instance2 = SingletonUnity.Instance;
            instance.TestSingleTon();
        }
    }
}