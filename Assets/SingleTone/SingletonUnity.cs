using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingletonPattern
{
    public class SingletonUnity : MonoBehaviour
    {
        private static SingletonUnity instance = null;

        public static SingletonUnity Instance
        {
            get
            {
                if(instance == null)
                {
                    SingletonUnity[] allsingletonsInScene = GameObject.FindObjectsOfType<SingletonUnity>(); //씬 안에서 싱글톤 유니티 타입을 다 찾음.

                    if (allsingletonsInScene != null && allsingletonsInScene.Length > 0)
                    {
                        if (allsingletonsInScene.Length > 1) //두개 이상.
                        {
                            Debug.LogWarning("Already Define SingleTone in the Scene");

                            for (int i = 1; i < allsingletonsInScene.Length; i++)
                            {
                                Destroy(allsingletonsInScene[i].gameObject);
                            }
                        }

                        instance = allsingletonsInScene[0];

                        //초기화
                        instance.FakeConstructor();
                    }
                    else
                    {
                        Debug.LogError($"Not Define SIngleTone Object");
                    }
                }
                return instance;
            }
        }

        private float randomNumber;
        private void FakeConstructor() //생성자가 없는데 생성자처럼 쓰는것.
        {
            randomNumber = Random.Range(0f, 1f);
        }

        public void TestSingleTon()
        {
            Debug.Log($"Hello Singleton, Random number is {randomNumber}");
        }
    }
}