using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonCsharp 
{
    private static SingletonCsharp instnace = null;

    public static SingletonCsharp Instance
    {
        get
        {
            if (instnace == null)
            {
                instnace = new SingletonCsharp();
            }

            return instnace;
        }
    }


    private float randomNumber;

    private SingletonCsharp()
    {
        randomNumber = Random.Range(0f, 1f);
    }

    public void TestSingleTon()
    {
        Debug.Log($"Hello Singleton, Random number is {randomNumber}");
    }
}
