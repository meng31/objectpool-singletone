using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ObjectPolling.Single
{
    public class GunController : MonoBehaviour
    {
        public BulletObjectPool bulletPool;
        private float rotationSpeed = 60f;
        private float fireTimer;
        private float fireInterval = 0.1f;

        void Start()
        {
            fireTimer = Mathf.Infinity; // float형 을초기화할때.

            if(bulletPool == null)
            {
                Debug.LogError("Need a reference to the object pool");
            }
        }

        void Update()
        {
            fireTimer += Time.deltaTime;

            if (Input.GetKey(KeyCode.A))
            { this.transform.Rotate(Vector3.up, -rotationSpeed * Time.deltaTime); }
            else if (Input.GetKey(KeyCode.D))
            { this.transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime); }

            //Fire gun
            if (Input.GetKey(KeyCode.Space) && fireTimer > fireInterval)
            {
                fireTimer = 0f;
                GameObject newBullet = GetABullet();
                if(newBullet != null)
                {
                    newBullet.SetActive(true);
                    newBullet.transform.forward = this.transform.forward;
                    newBullet.transform.position = this.transform.position + transform.forward * 2f;
                }
                else
                {
                    Debug.Log("Couldn't find a new Bullet");
                }
                

            }
        }
        private GameObject GetABullet()
        {
            GameObject bullet = bulletPool.GetBullet();
            return bullet;
        }
    }
}