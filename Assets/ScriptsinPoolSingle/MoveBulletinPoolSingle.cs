using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBulletinPoolSingle : MonoBehaviour
{
    private float bulletSpeed = 10f;
    private float deactivationDistance = 15f; // 비활성화 하는 거리

    private void Update()
    {
        transform.Translate(Vector3.forward * bulletSpeed * Time.deltaTime);

        if(Vector3.SqrMagnitude(transform.position) > deactivationDistance * deactivationDistance)
        {
            gameObject.SetActive(false);
        }
    }
}
