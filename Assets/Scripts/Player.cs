using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public string bulletName = "Bullet";

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
    }

    void Shoot()
    {
        GameObject bullet = ObjectPool.Instance.PopFromPool(bulletName);
        bullet.transform.position = transform.position + transform.up;
        bullet.SetActive(true);
    }
}
