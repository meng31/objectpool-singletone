using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : Singleton<ObjectPool>
{
    //pooledObject 클래스를 리스트로 관리하는 변수
    public List<PooledObject> objectPool = new List<PooledObject>();

    private void Awake()
    {
        for (int ix= 0; ix<objectPool.Count; ++ix)
        {
            objectPool[ix].Initialize(this.transform);
        }
    }

    PooledObject getPoolItem(string itemName)
    {
        //이 함수는 전달된 itemName 파라미터와 같은 이름을 가진 오브텍트 풀을 검색하고, 검색에 성공하면 그 결과 값을 반환한다.
        for(int ix=0; ix<objectPool.Count; ++ix)
        {
            if (objectPool[ix].poolItemName.Equals(itemName))
                return objectPool[ix];
        }

        Debug.LogWarning("there's no matched pool list");
        return null;
    }


    public bool pushToPool(string itemName, GameObject item, Transform parent = null)
    {
        //사용한 객체를 풀에 반환할때
        PooledObject pool = getPoolItem(itemName);
        if (pool == null)
            return false;

        pool.PushToPool(item, parent == null ? this.transform : parent);
        return true;
    }

    public GameObject PopFromPool(string itemName, Transform parent = null)
    {
        //필요한 객체를 오브젝트 풀에 요청할때 사용.
        PooledObject pool = getPoolItem(itemName);
        if (pool == null)
            return null;

        return pool.PopFromPool(parent);
    }
}
