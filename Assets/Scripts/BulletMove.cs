using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    public string poolItemName = "Bullet"; // 오브젝트 풀에 저장된 bullet 오브젝트 이름.
    public float moveSpeed = 10f;
    public float lifeTime = 3f;
    private float _elapsedTime = 0f;

    private void Update()
    {
        transform.position += transform.up * moveSpeed * Time.deltaTime;

        if (GetTimer() > lifeTime)
        {
            SetTimer();
            ObjectPool.Instance.pushToPool(poolItemName, gameObject);
        }
    }

    float GetTimer()
    {
        return (_elapsedTime += Time.deltaTime);
    }

    void SetTimer()
    {
        _elapsedTime = 0f;
    }

}
