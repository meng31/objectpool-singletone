using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour // 반드시 모노비헤이비어를 상속받은 녀석만 T가될수 있다.
{
    private static T _instance;

    public static T Instance
    {
        get
        {
            if(_instance ==null)
            {
                _instance = FindObjectOfType(typeof(T)) as T;

                if(_instance == null)
                {
                    Debug.LogError("there's no active" + typeof(T) + " in this scene");
                }
            }

            return _instance;
        }
    }
}
