using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectPolling.Multi
{
    public class TutorialPoolMulti : MonoBehaviour
    {
        Vector3 spawnnPosition;

        private void Start()
        {
            StartCoroutine(SpawnObjects());
        }

        IEnumerator SpawnObjects()
        {
            while (true)
            {
                GameObject go;
                float xValue = Random.Range(-5f, 5f);
                spawnnPosition = new Vector3(xValue, 2f, -0.5f);

                go = PoolManagerMulti.instance.Spawn("Cube", spawnnPosition, Quaternion.identity);

                if (go != null)
                {
                    ApplyForce(go.transform);
                    PoolManagerMulti.instance.Despawn(go, 2f);
                }

                yield return new WaitForSeconds(0.1f);
                xValue = Random.Range(-5f, 5f);
                go = PoolManagerMulti.instance.Spawn("Sphere", spawnnPosition, Quaternion.identity);

                if (go != null)
                {
                    ApplyForce(go.transform);
                    PoolManagerMulti.instance.Despawn(go, 2f);
                }

                yield return new WaitForSeconds(0.01f);
            }
        }



        public void ApplyForce(Transform target)
        {
            Rigidbody rb = target.GetComponent<Rigidbody>();

            if (!rb) return;

            rb.velocity = Vector3.zero;
            rb.AddForce(transform.up * 200.0f);
            rb.useGravity = true;
        }
    }
}