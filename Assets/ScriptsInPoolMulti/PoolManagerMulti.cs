using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectPolling.Multi
{
    /// <summary>
    /// 이 클래스는 오브젝트 풀의 기본 속성을 정의 한다.
    /// 현재는 오브젝트 풀에서 사용할 프리팹과 크기를 저장한다.
    /// </summary>
    [System.Serializable]
    public class ObjectPool
    {
        public GameObject prefab;
        public int size;
    }

    public class PoolManagerMulti : MonoBehaviour
    {
        public List<ObjectPool> ObjectPoolList;

        private Dictionary<string, Queue<GameObject>> objectPoolDictionary;

        #region SingleTone
        private static PoolManagerMulti _instance = null;

        public static PoolManagerMulti instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = (PoolManagerMulti)FindObjectOfType(typeof(PoolManagerMulti));
                    if(_instance == null)
                    {
                        Debug.Log("There's no active Singletone object");
                    }
                }

                return _instance;
            }
        }

        private void Awake()
        {
            if(_instance !=null && _instance != this)
            {
                DestroyImmediate(gameObject);
            }
            else
            {
                _instance = this;
                init();
            }
        }
        #endregion

        GameObject CreateGameObject(GameObject prefab)
        {
            GameObject obj = GameObject.Instantiate(prefab);
            obj.name = prefab.name;
            obj.SetActive(false);
            obj.transform.parent = this.transform;
            return obj;
        }

        ObjectPool FindObjectPool (string PoolName)
        {
            return ObjectPoolList.Find(x => (x.prefab != null && string.Equals(x.prefab.name, PoolName))); //풀 리스트 배열안에 오브젝트 풀중에서 조건을 만족하는 오브젝트풀 클래스를 꺼내오는것.
        }


        private void init()
        {
            objectPoolDictionary = new Dictionary<string, Queue<GameObject>>();
            foreach(var pool in ObjectPoolList)
            {
                if (!pool.prefab)
                {
                    Debug.LogError("invaild prefab");
                    continue;
                }
                //위 조건식이 거짓이면(프리팹이 있는 오브젝트풀클래스 라면)
                //(오브젝트 풀)큐를 만들고 여기에 현재(지금for문을돌고있는거니까)오브젝트풀 클래스에 정의된 size만큼 오브젝트(pool.prfab)를 저장한다.
                Queue<GameObject> poolQueue = new Queue<GameObject>();
                for(int i = 0; i < pool.size; i++)
                {
                    poolQueue.Enqueue(CreateGameObject(pool.prefab));
                }

                //dictionary에 오브젝트 풀 추가
                objectPoolDictionary.Add(pool.prefab.name, poolQueue);
            }
        }

        /// <summary>
        /// 오브젝트 풀에서 오브젝트 가져오기
        /// 오브젝트의 위치와 회전을 설정한다.
        /// </summary>
        public GameObject Spawn(string poolName, Vector3 position, Quaternion rotation)
        {
            GameObject obj = null;

            if(objectPoolDictionary.ContainsKey(poolName))
            {
                Queue<GameObject> poolQueue/*배열*/ = objectPoolDictionary[poolName];

                if(poolQueue.Count > 0)
                {
                    // 오브젝트 풀에 오브젝트가 있을때 디큐로 반환한다.
                    obj = poolQueue.Dequeue();
                }
                else
                {
                    //매개인자인 pool Name을 가진 오브젝트 풀이 비어있다.
                    ObjectPool pool = FindObjectPool(poolName);
                    if (pool != null)
                    {
                        obj = CreateGameObject(pool.prefab);
                    }
                    
                }

                if (obj != null) // setTransform이라고 생각하면됑
                {
                    obj.transform.position = position;
                    obj.transform.rotation = rotation;
                    obj.SetActive(true);
                }
            }
            else
            {
                Debug.LogError(poolName + " object Pool is not available");
            }

            return obj;
        }

        /// <summary>
        /// 오브젝트 풀에 오브젝트를 반환하는것.
        /// </summary>
        private IEnumerator _Despawn (GameObject poolObject, float timer)
        {
            yield return new WaitForSeconds(timer); // 인터벌이 있기때문에 밑의 if문(방어코드)가 있는것이다.

            if(objectPoolDictionary.ContainsKey(poolObject.name))
            {
                Queue<GameObject> poolQueue = objectPoolDictionary[poolObject.name];

                if(poolQueue.Contains(poolObject) == false) // 오브젝트 풀에 이 오브젝트가 이미 비활성화되어 있는지 검사한다.
                {
                    objectPoolDictionary[poolObject.name].Enqueue(poolObject);
                    poolObject.SetActive(false);
                }
                else
                {
                    //이미처리됨
                    Debug.LogWarning($"Already despawn {poolObject.name}");
                }
            }
            else
            {
                Debug.LogError(poolObject.name + " object pool is not available");
            }
        }

        public void Despawn(GameObject poolObject, float timer = 0f)
        {
            StartCoroutine(_Despawn(poolObject, timer)); // 한번 감싸는 랩핑
        }
    }
}

